function kimpul_getMeta(onSuccess,onError){
	console.log("kimpul_getMeta");
	var pload = {};

	var onSuccess_i = function (payload) {
		if (payload.full_name != null) { 
			window.localStorage.setItem("full_name", payload.full_name); 
		}

		if (payload.mobile_phone != null) { 
			window.localStorage.setItem("mobile_phone", payload.mobile_phone); 
		}

		if (payload.email != null) { 
			window.localStorage.setItem("email", payload.email); 
		}

		if (payload.address != null) { 
			window.localStorage.setItem("address", payload.address); 
		}

		if (payload.city != null) { 
			window.localStorage.setItem("city", payload.city); 
		}

		if (payload.province != null) { 
			window.localStorage.setItem("province", payload.province); 
		}

		if (payload.country != null) { 
			window.localStorage.setItem("country", payload.country); 
		}

		if (payload.zip != null) { 
			window.localStorage.setItem("zip", payload.zip); 
		}

		if (payload.birth_place != null) { 
			window.localStorage.setItem("birth_place", payload.birth_place); 
		}

		if (payload.birth_date != null) { 
			window.localStorage.setItem("birth_date", payload.birth_date); 
		}

		if (payload.email != null) { 
			window.localStorage.setItem("email", payload.email); 
		}

		if (payload.id_number != null) { 
			window.localStorage.setItem("id_number", payload.id_number); 
		}

		if (payload.kelurahan != null) { 
			window.localStorage.setItem("kelurahan", payload.kelurahan); 
		}

		if (payload.kecamatan != null) { 
			window.localStorage.setItem("kecamatan", payload.kecamatan); 
		}

		if (payload.fb_token != null) { 
			window.localStorage.setItem("fb_token", payload.fb_token); 
		}

		if (payload.fb_id != null) { 
			window.localStorage.setItem("fb_id", payload.fb_id); 
		}

		if (payload.twitter_token != null) { 
			window.localStorage.setItem("twitter_token", payload.twitter_token); 
		}

		if (payload.verified != null) { 
			window.localStorage.setItem("verified", payload.verified); 
		}
		else {
			window.localStorage.setItem("verified", "Pending"); 
		}

		if (payload.verification_notes != null) { 
			window.localStorage.setItem("verification_notes", payload.verification_notes); 
		}
		else {
			window.localStorage.setItem("verification_notes", ""); 
		}

		if (payload.id_pic!= null) { 
			moffas_clear_image_data("id");
			var appUrl = window.localStorage.getItem("appUrl");
			var sessionId = window.localStorage.getItem("sessionId");
			window.localStorage.setItem("id_path", appUrl+"/id_pic.php?sid="+sessionId); 
		}

		if (payload.home_pic!= null) { 
			moffas_clear_image_data("home");
			var appUrl = window.localStorage.getItem("appUrl");
			var sessionId = window.localStorage.getItem("sessionId");
			window.localStorage.setItem("home_path", appUrl+"/home_pic.php?sid="+sessionId); 
		}

		if (payload.profile_pic!= null) { 
			moffas_clear_image_data("profile");
			var appUrl = window.localStorage.getItem("appUrl");
			var sessionId = window.localStorage.getItem("sessionId");
			window.localStorage.setItem("profile_path", appUrl+"/profile_pic.php?sid="+sessionId); 
		}
		onSuccess(payload);
	}
	moffas_do("get_prof",pload,onSuccess_i,onError);
}

function kimpul_updateProfile(pload,onSuccess,onError){
	console.log("kimpul_updateProfile");
	var payload={};
	for(var key in pload) {
		var value = pload[key];
		if (value instanceof Date) {
			var str = moffas_Date_toYMD(value);
			value = str;
		}
		payload[key] = value;
		window.localStorage.setItem(key, value); 
	}
	console.log(payload);
	moffas_do("upd_prof",payload,onSuccess,onError);
}


function Kimpul_updateProfilePic(prefix,onSuccess,onError){
	console.log("Kimpul_updateProfilePic");
	var filekey = prefix+"_pic";
	var updated =  window.localStorage.getItem(prefix+"_updated");
	if (updated != "1") {
		e = new Error("No Picture found.");
		onError(e);
	}
	else {
		var mimetype = window.localStorage.getItem(prefix+"_mimetype");
		var filepath = window.localStorage.getItem(prefix+"_path");
		var checksum = window.localStorage.getItem(prefix+"_hash");
		var long = window.localStorage.getItem(prefix+"_"+"long");
		var lat = window.localStorage.getItem(prefix+"_"+"lat");
		var acc = window.localStorage.getItem(prefix+"_"+"acc");
		var timestamp = window.localStorage.getItem(prefix+"_"+"timestamp");
		var pload = {};
		pload[prefix+"_"+"long"] =  long;
		pload[prefix+"_"+"lat"] =  lat;
		pload[prefix+"_"+"acc"] =  acc;
		pload[prefix+"_"+"timestamp"] =  timestamp;
		function processEntry(entry) {
			console.log("processing picture :".filepath);
			function cf_win(file) {
				var reader = new FileReader();
				console.log("verifying checksum.");
				reader.onloadend = onLoadEnd;
				function onLoadEnd(evt) {
					var md5sum = md5(new Uint8Array(evt.target.result));
					if (checksum == md5sum) {
						function onSuccess_i(payload){
							window.localStorage.setItem(prefix+"_updated","0");
							onSuccess(payload);
						}
						console.log("checksum matched");
						moffas_do_with_file("upd_prof",pload,filekey,mimetype,filepath,onSuccess_i,onError);
					}
					else {
						console.log("checksum error");
						e = new Error("Image Checksum Error");
						moffas_clear_image_data(prefix);
						onError(e);
					}
				}
				reader.readAsArrayBuffer(file);
			}
			entry.file(cf_win, onError);
		}
		resolveLocalFileSystemURL(filepath, processEntry ,onError);
	}
}


function kimpul_report(payload,onSuccess,onError){
	console.log("kimpul_report");
	var prefix = "report";
	var filekey = prefix+"_pic";
	var updated =  window.localStorage.getItem(prefix+"_updated");
	var kimpulid = window.localStorage.getItem("kimpul_id");
	if (updated != "1") {
		e = new Error("No Picture found.");
		onError(e);
	}
	else {
		var mimetype = window.localStorage.getItem(prefix+"_mimetype");
		var filepath = window.localStorage.getItem(prefix+"_path");
		var checksum = window.localStorage.getItem(prefix+"_hash");
		var long = window.localStorage.getItem(prefix+"_"+"long");
		var lat = window.localStorage.getItem(prefix+"_"+"lat");
		var acc = window.localStorage.getItem(prefix+"_"+"acc");
		var mcc = window.localStorage.getItem(prefix+"_"+"mcc");
		var mnc = window.localStorage.getItem(prefix+"_"+"mnc");
		var lac = window.localStorage.getItem(prefix+"_"+"lac");
		var cid = window.localStorage.getItem(prefix+"_"+"cid");
		var timestamp = window.localStorage.getItem(prefix+"_"+"timestamp");
		payload[prefix+"_"+"long"] =  long;
		payload[prefix+"_"+"lat"] =  lat;
		payload[prefix+"_"+"acc"] =  acc;
		payload[prefix+"_"+"mcc"] =  mcc;
		payload[prefix+"_"+"mnc"] =  mnc;
		payload[prefix+"_"+"lac"] =  lac;
		payload[prefix+"_"+"cid"] =  cid;
		payload[prefix+"_"+"timestamp"] =  timestamp;
		payload["kimpulid"] = kimpulid;
		console.log("filepath : "+filepath);
		function processEntry(entry) {
			console.log("processing file");
			console.log(entry);
			function cf_win(file) {
				var reader = new FileReader();
				reader.onloadend = function(evt) {
					console.log("verifying file checksum");
					console.log(evt);
					var md5sum = md5(new Uint8Array(evt.target.result));
					if (checksum == md5sum) {
						console.log("checksum match");
						var c = {
							payload: payload,
							filekey: filekey,
							mimetype: mimetype,
							filepath: filepath,
						}
						console.log(c);
						moffas_do_with_file("report",payload,filekey,mimetype,filepath,onSuccess,onError);
					}
					else {
						console.log("checksum error");
						e = new Error("Image Checksum Error");
						onError(e);
					}
				};
				reader.readAsArrayBuffer(file);
			}
			entry.file(cf_win, onError);
		}
		resolveLocalFileSystemURL(filepath, processEntry ,onError);
	}
}


function kimpul_check_login(){
	console.log("kimpul_check_login");
	var username = window.localStorage.getItem("username");
	if (username === undefined) {
		console.log("username undefined.");
		return "login";
	}
	var logged_in = window.localStorage.getItem("logged_in");
	if (logged_in !== "1") {
		console.log("not logged in.");
		// window.location = "index.html/#/login";
		return "login";
	}
	return "success";
}

function kimpul_init_error(error){
	console.log("kimpul_init_error");
	SpinnerPlugin.activityStop();
	console.error("The following error occurred: "+error);
	function alertDismissed() {
		navigator.app.exitApp();
	}
	navigator.notification.alert(
		'Kimpulku Initialization Error',  // message
		alertDismissed,         // callback
		'Init Error',            // title
		'OK'                  // buttonName
	);
}

function kimpul_init(){
	console.log("kimpul_init");
	var appId = "moffas"; // "talangin"; // "kimpulku";
	var appSecret = "mumumu"; //"t4Lang!N"; //"k1mP0y";
	// var url = "http://telakses3.dyndns.org:21880/moffas/webservice";
	// var weburl = "http://telakses3.dyndns.org:21880/moffas/";
	var url = "http://159.89.207.173/moffasapp/webservice";
	var weburl = "http://159.89.207.173/moffasapp/";
	window.localStorage.removeItem("full_name");
	window.localStorage.setItem("webUrl",weburl);
	moffas_init_with_firebase(appId,appSecret,url,initSuccess,kimpul_init_error);

	function initSuccess() {
		SpinnerPlugin.activityStop();
		// window.location = "main.html";
	}


}

function kimpul_onErrorGPS(error) {
	console.log("kimpul on GPS Error");
	SpinnerPlugin.activityStop();
	console.error("The following error occurred: "+error);
	function alertDismissed() {
		navigator.app.exitApp();
	}
	navigator.notification.alert(
		'GPS Activation error',  // message
		alertDismissed,         // callback
		'GPS Error',            // title
		'OK'                  // buttonName
	);
}

function kimpul_enableGPS(onSuccess,onError){
	console.log("kimpul_enable GPS");
	cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
		console.log("GPS location is " + (enabled ? "enabled" : "disabled"));
		if (!enabled) {
			function onRequestSuccess(success){
				console.log("Successfully requested accuracy: "+success.message);
				onSuccess();
			}

			function onRequestFailure(error){
				console.error("Accuracy request failed: error code="+error.code+"; error message="+error.message);
				if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
					if(window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
						cordova.plugins.diagnostic.switchToLocationSettings();
						enableGPS(onSuccess,onError);
					} else {
						onError(error);
					}
				}
				else {
					onError(error);
				}
			}
			cordova.plugins.locationAccuracy.request(onRequestSuccess, onRequestFailure, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
		}
		else {
			onSuccess();
		}
	}, function(error){
		onError(error);
	}); 	
}

function kimpul_start() {
	console.log("kimpul_onDeviceReady start");
	var optionsspinner = { dimBackground: true };
	SpinnerPlugin.activityStart("Initializing...", optionsspinner);


	var permissions = cordova.plugins.permissions;

	function checkPermissionCallback(status) {
		console.log(status);
		if (!status.hasPermission) {
			var errorCallback = function() {
				SpinnerPlugin.activityStop();
				console.warn('error gps permissions');
				function alertDismissed() {
					navigator.app.exitApp();
				}
				navigator.notification.alert(
					'Permission to access GPS denied.',  // message
					alertDismissed,         // callback
					'Permission Denied',            // title
					'OK'                  // buttonName
				);
			}

			permissions.requestPermission(
				permissions.ACCESS_FINE_LOCATION,
				function(status) {
					if (!status.hasPermission) {
						errorCallback();
					}
					else {
						kimpul_enableGPS(kimpul_init,kimpul_onErrorGPS);
					}
				},
				errorCallback
			);
		}
		else {
			kimpul_enableGPS(kimpul_init,kimpul_onErrorGPS);
		}
	}
	permissions.checkPermission(permissions.ACCESS_FINE_LOCATION, checkPermissionCallback, null);
}
