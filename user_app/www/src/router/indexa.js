import Vue from 'vue'
import Router from 'vue-router'

// const Foo = resolve => require(['../views/hello.vue'], resolve);
// import hello from '../views/hello.vue';
import home from '../views/home.vue'
import mainmenu from '../views/mainmenu.vue'
import login from '../views/login.vue'


Vue.use(Router)

export default new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    { path: '/', component: login },
    { path: '/home', component: mainmenu },
    // {
    //   path: '/detail/:id',
    //   component: detail,
    //   props : true
    // },
    { name: 'login', path: '/login', component: login },
    ]
})
