import Vue from 'vue'
import Router from 'vue-router'
import qr_scan from '../views/qr_scan.vue'
import redir from '../views/Redir.vue'
import mainmenu from '../views/Mainmenu.vue'
import login from '../views/Login.vue'
import regis from '../views/Register.vue'
import cregis from '../views/ConfirmRegister.vue'
import payment from '../views/Payment.vue'
import prelog from '../views/Prelog.vue'
import pin from '../views/Pin.vue'
import lupapin from '../views/ForgetPin.vue'
import pinotp from '../views/PinOTP.vue'
import newpin from '../views/NewPin.vue'
import clogin from '../views/ConfirmLogin.vue'
import topup from '../views/topup.vue'
import transfer from '../views/Transfer.vue'
import ctransfer from '../views/confirmTransfer.vue'
import ctransferbank from '../views/confirmTransferBank.vue'
import transferSuccessBank from '../views/transferSuccessBank.vue'
import pintransfer from '../views/TransferPin.vue'
import transferSuccess from '../views/transferSuccess.vue'
import staticTransaction from '../views/StaticTransaction.vue'
import csPayment from '../views/ConfirmStaticPayment.vue'
import paymentSuccess from '../views/PaymentSuccess.vue'
import pinPayment from '../views/PinPayment.vue'
import profile from '../views/Profile/Profile.vue'
import editprofile from '../views/Profile/ProfileEdit.vue'
import editphone from '../views/Profile/EditPhone.vue'
import profileotp from '../views/Profile/ProfileOTP.vue'
import upgrade from '../views/Profile/ProfileUpgrade.vue'
import upgradeConfirm from '../views/Profile/UpgradeConfirm.vue'
import profilephoto from '../views/Profile/ProfilePhoto.vue'
import about from '../views/about.vue'
import payamount from '../views/payamount.vue'
import cpayment from '../views/ConfirmPayment.vue'
import settings from '../views/Settings/Settings.vue'
import faq from '../views/Settings/Faq.vue'
import ubahpin from '../views/Settings/NewPin.vue'
import notification from '../views/Notifikasi.vue'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/',name: 'redir',component: redir },
    { path: '/cpayment/:pload',name: 'cpayment',component: cpayment, props:true ,meta:{title:'KONFIRMASI PEMBAYARAN',showback:true} },
    { path: '/payamount/:pload',name: 'payamount',component: payamount, props:true },
    { path: '/qr_scan',name: 'qr_scan',component: qr_scan },
    { path: '/prelog',name: 'prelog',component: prelog },
    { path: '/home',name:'D-Money', component: mainmenu, meta:{title:'D-PAY',showback:false} },
    { path: '/regis/:phone',name:'pendaftaran', component: regis,props:true, meta:{title:'pendaftaran',showback:true} },
    { path: '/pin', component: pin },
    { path: '/clogin', component: clogin },
    { path: '/topup', name:'TOP UP',component: topup, meta:{title:'TOP UP',showback:true} },
    { path: '/transfer', name:'TRANSFER',component: transfer },
    { path: '/ctransfer', name:'KONFIRMASI TRANSFER',component: ctransfer },
    { path: '/ctransferbank', name:'KONFIRMASI TRANSFER BANK',component: ctransferbank,meta:{title:'KONFIRMASI TRANSFER',showback:true} },
    { path: '/pintransfer/', props: true, name:'KONFIRMASI TRANSFER2',component: pintransfer, meta:{title:'KONFIRMASI TRANSFER',showback:true} },
    { path: '/pintransfer/:bank', props: true, name:'KONFIRMASI TRANSFER2',component: pintransfer, meta:{title:'KONFIRMASI TRANSFER',showback:true} },
    { path: '/transferSuccess', name:'KONFIRMASI BERHASIL BANK',component: transferSuccess,meta:{title:'BERHASIL',showback:false} },
    { path: '/transferSuccessBank', name:'TRANSFER BERHASIL',component: transferSuccessBank, meta:{title:'BERHASIL',showback:false} },
    { path: '/payment/:payload',name:'pembayaran', component: payment },
    { path: '/cregis/:pload', component: cregis, props: true },
    { path: '/staticTransaction/:code',name: 'Pembayaran', component: staticTransaction, props: true,meta:{title:'Pembayaran',showback:true} },
    { path: '/csPayment/:code',name: 'Pembayaran', component: csPayment, props: true,meta:{title:'Konfirmasi Pembayaran',showback:true} },
    { path: '/pinPayment',name: 'Pembayaran', component: pinPayment, props: true,meta:{title:'Konfirmasi Pembayaran',showback:true} },
    { path: '/paymentSuccess',name: 'Pembayaran', component: paymentSuccess,meta:{title:'BERHASIL',showback:false} },
    { path: '/login',name: 'Selamat Datang',  component: login, meta:{title:'Selamat Datang'} },
    { path: '/lupapin',name: 'Lupa Pin',  component: lupapin, meta:{title:'Lupa Pin',showback:true} },
    { path: '/pinotp',name: 'Lupa Pin ',  component: pinotp, meta:{title:'Lupa Pin',showback:true} },
    { path: '/profile',name: 'Profile',  component: profile, meta:{title:'D-Pay',showback:false} },
    { path: '/editprofile',name: 'Edit Profile',  component: editprofile, meta:{title:'Edit Profile',showback:true} },
    { path: '/editphone',name: 'Edit Phone',  component: editphone, meta:{title:'Ubah No. Ponsel',showback:true} },
    { path: '/profileotp/:pload',name: 'Edit Phone ' , props: true,  component: profileotp, meta:{title:'Ubah No. Ponsel',showback:true} },
    { path: '/upgrade',name: 'Upgrade Akun ' , props: false,  component: upgrade, meta:{title:'Upgrade Akun',showback:true} },
    { path: '/upgradeConfirm',name: 'Upgrade Akun  ' , props: false,  component: upgradeConfirm, meta:{title:'Konfirmasi',showback:true} },
    { path: '/profilephoto',name: 'Upgrade Akun ' , props: false,  component: profilephoto, meta:{title:'Upgrade Akun',showback:true} },
    { path: '/newpin',name: 'Lupa Pin',  component: newpin, meta:{title:'Lupa Pin',showback:true} },
    { path: '/settings',name: 'Pengaturan',  component: settings, meta:{title:'Pengaturan',showback:true} },
    { path: '/faq',name: 'FAQ ',  component: faq, meta:{title:'FAQ',showback:true} },
    { path: '/ubahpin',name: 'Ubah Pin ',  component: ubahpin, meta:{title:'Ubah Pin',showback:true} },
    { path: '/notification',name: 'Notifikasi ',  component: notification, meta:{title:'Notifikasi',showback:true} },
  ]
})
