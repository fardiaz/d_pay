- create project :
cordova create nasabah_app --template=cordovue

- build:
yarn build
cordova build android

- plugins:
cordova-plugin-camera
cordova-plugin-compat
cordova-plugin-device
cordova-plugin-dialogs
cordova-plugin-geolocation
cordova-plugin-qrscanner
cordova-plugin-screen-orientation
cordova-plugin-spinner
cordova-plugin-whitelist
