document.addEventListener("deviceready", yourCallbackFunction, false);

function yourCallbackFunction(){
    console.log("qr scan");
    window.localStorage.setItem("moffas.redir","/home");
    QRScanner.show(function(status){
      console.log(status);
    });
    QRScanner.scan(displayContents);
    function displayContents(err, text){
      if(err){
        // an error occurred, or the scan was canceled (error code `6`)
        console.log(err);
      } else {
        // The scan completed, display the contents of the QR code:
        window.localStorage.setItem("moffas.scanned_qr",text);
        //window.location="index.html";
      }
    }
/*    
       cordova.plugins.barcodeScanner.scan(moffas.handleQR,moffas.qrError,
      {
          preferFrontCamera : true, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: true, // Android, launch with the torch switched on (if available)
          saveHistory: true, // Android, save scan history (default false)
          prompt : "Place a barcode inside the scan area", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS and Android
      }
    );
*/

}
