var moffas = moffas || {};

moffas.scram = {};

moffas.scram.generateClientFirst =  function(user) {
  var nonce = moffas.util.generateNonce(16);
  var text ='n,,n='+user+',r='+nonce;
  return [nonce,text];
}

moffas.scram.verifyServerFirst =  function(nonce,serverfirst){
  var s1c = serverfirst.split(',')
  while((i=s1c.shift())!== undefined ) {
    var iname=i.substr(0,1);
    var ival =i.substr(2);
    if (iname == 'r') {
      snonce = ival;
    }
  }
  // check nonce
  n = nonce.length;
  if (nonce != snonce.substr(0,n)) return false;
  return true;
}

moffas.scram.generateClientFinal = function(otp,password,clientfirst,serverfirst) {
  var aeskey;
  var text;
  var ss;

  var c1c = clientfirst.split(',');
  var gh = c1c.shift()+",";
  gh += c1c.shift()+',';
  var c1b = c1c.join(',');
  var s1c = serverfirst.split(',');
  var nonce='';
  var salt = '';
  var iter=4096;

  while((i=s1c.shift())!== undefined ) {
    var iname=i.substr(0,1);
    var ival =i.substr(2);
    if (iname == 's') {
      salt = CryptoJS.enc.Base64.parse(ival);
    }
    else if (iname == 'r') {
      nonce = ival;
    }
    else if (iname == 'i') {
      iter = parseInt(ival);
    }
  }
  var c2 = 'c='+btoa(gh)+',r='+nonce
  var AuthMessage = c1b+','+serverfirst+','+c2;

  var SaltedPassword = null;
  if (otp && password) {
    console.log("otp + password authentication");
    var sp1 = CryptoJS.PBKDF2(password, salt, {
      keySize: 8,
      hasher:     CryptoJS.algo.SHA256,
      iterations: iter
    });
    SaltedPassword = CryptoJS.PBKDF2(otp, sp1, {
      keySize: 8,
      hasher:     CryptoJS.algo.SHA256,
      iterations: iter
    });
  }
  else if (password) {
    console.log("password authentication");
    SaltedPassword = CryptoJS.PBKDF2(password, salt, {
      keySize: 8,
      hasher:     CryptoJS.algo.SHA256,
      iterations: iter
    });
  }
  else if (otp) {
    console.log("otp authentication");
    SaltedPassword = CryptoJS.PBKDF2(otp, salt, {
      keySize: 8,
      hasher:     CryptoJS.algo.SHA256,
      iterations: iter
    });
  }
  else {
    console.log("no password or otp. cannot proceed.");
    return false;
  }



  var sp64 = CryptoJS.enc.Base64.stringify(SaltedPassword);
  var ClientKey = CryptoJS.HmacSHA256("Client Key",SaltedPassword);
  var ck64 = CryptoJS.enc.Base64.stringify(ClientKey);
  var StoredKey = CryptoJS.SHA256(CryptoJS.enc.Base64.parse(ck64));
  var sk64 = CryptoJS.enc.Base64.stringify(StoredKey);
  var ClientSignature = CryptoJS.HmacSHA256(AuthMessage,StoredKey);
  var cs64 = CryptoJS.enc.Base64.stringify(ClientSignature);
  var cp64 = moffas.util.base64xor(ck64,cs64);
  var ServerKey = CryptoJS.HmacSHA256("Server Key",SaltedPassword);
  var svk64 = CryptoJS.enc.Base64.stringify(ServerKey);
  var ServerSignature = CryptoJS.HmacSHA256(AuthMessage,ServerKey);
  var ss64 = CryptoJS.enc.Base64.stringify(ServerSignature);

  console.log("sp : "+sp64);
  console.log("AuthMessage : ["+AuthMessage+"]");
  console.log("ck : "+ck64);
  console.log("sk : "+sk64);
  console.log("cs : "+cs64);
  console.log("cp : "+cp64);
  console.log("svk : "+svk64);
  console.log("ss : "+ss64);

  text = c2+",p="+cp64,
  ss = ss64;
  var aeskey = CryptoJS.PBKDF2(nonce, SaltedPassword, {
    keySize: 8,
    hasher:     CryptoJS.algo.SHA256,
    iterations: iter
  });
  var aesk64 = CryptoJS.enc.Base64.stringify(aeskey);
  console.log("aesk : "+aesk64);
  return [text,ss64,aesk64]
}

moffas.scram.verifyServerFinal = function(ss,serverfinal) {
  if (serverfinal == ('v='+ss)) {
    return true;
  } else {
    return false;
  }
}

// =======================================================================//
moffas.scram.start = function (user, auth_type, onSuccess, onError) {
    var url = moffas.config.url + "/auth";
  var res = moffas.scram.generateClientFirst(user);
  var nonce = res[0];
  var clientfirst = res[1];

  var data = {
    "data" : clientfirst,
    "AuthType" : auth_type,
  };

  var jdata = JSON.stringify(data);
  console.log('First jdata : '+jdata);

  var inError =  function(status,errortext){
    console.log("inError");
    console.log(status);
    console.log(errortext);
    if (status == 400) {
      console.log("400: malformed data");
      onError(Error("malformed data"));
    }
    else if (status == 401) {
      console.log("401 : unauthenticated.");
      window.localStorage.removeItem("moffas.token");
      onError(Error("unauthenticated"));
    }
    else if (status == 403) {
      console.log("403:Denied");
      window.localStorage.removeItem("moffas.token");
      onError(Error("denied"));
    }
    else if (status == "parseerror") {
      console.log("Invalid response.");
      onError(Error("invalid response"));
    }
    else {
      console.log("error : "+status+":"+errortext);
      onError(Error("error : "+status+":"+errortext));
    }
  }

  var inSuccess = function(data) {
    //var data  = JSON.parse(msg);
    console.log("inSuccess");
    console.log(data);
    try {
      var success = true;
      var serverfirst = data.response;
      if (moffas.scram.verifyServerFirst(nonce,serverfirst)) {
        window.localStorage.setItem("moffas.scram.nonce",nonce);
        window.localStorage.setItem("moffas.scram.clientfirst",res[1]);
        window.localStorage.setItem("moffas.scram.serverfirst",serverfirst);
        console.log(auth_type);
        window.localStorage.setItem("moffas.scram.type",auth_type);
        console.log("first success");
      }
      else {
        console.log("Invalid Nonce");
        onError("invalid nonce");
        success = false;
      }
    } catch(err) {
      console.log("Invalid Response : "+data.response);
      onError("invalid response");
      success = false;
    }
    if (success) {
        onSuccess(data);
    }

  }


  // EXECUTE HTTP REQUEST
  var exechttp = function(access_token) {
    var xhr = new XMLHttpRequest();
    var onReady = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          var response = xhr.responseText;
          var tocontinue = true;
          console.log("response :"+response);
          try {
            var j = JSON.parse(response);
          } catch(e) {
            console.log(e); // error in the above string (in this case, yes)!
            tocontinue=false;
            inError("parseerror","");
          }
          if (tocontinue) {
            inSuccess(j);
          }
        }
        else {
          inError(this.status,"HTTP ERROR");
        }
      }
    }
    xhr.onreadystatechange = onReady;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("authorization", "Bearer "+access_token);
    xhr.setRequestHeader("content-type", "application/json");
    var head = moffas.util.generateHeader(url,jdata);
    for (var k in head){
        xhr.setRequestHeader(k,head[k]);
    }
    xhr.send(jdata);
  }
  moffas.scram.getAccessToken(exechttp,onError);
}

// =======================================================================//
moffas.scram.finalize = function (otp,password,payload,onSuccess,onError) {
    var url = moffas.config.url + "/fin";
  var clientfirst = window.localStorage.getItem("moffas.scram.clientfirst");
  var serverfirst = window.localStorage.getItem("moffas.scram.serverfirst");
  var auth_type = window.localStorage.getItem("moffas.scram.type");
  var res = null;
  if (auth_type == "register" || auth_type == "forget2") {
    res = moffas.scram.generateClientFinal(otp,null,clientfirst,serverfirst);
  } else if (auth_type == "relogin") {
    res = moffas.scram.generateClientFinal(null,password,clientfirst,serverfirst);
  } else {
    res = moffas.scram.generateClientFinal(otp,password,clientfirst,serverfirst);
  }
  if (!res) {
    onError("fail to generate Final message");
    return;
  }

  var ss = res[1];
  var aesk64 = res[2];
  var scramdata = res[0];

  var res = moffas.util.encrypt(JSON.stringify(payload),aesk64,'0000');
  var pload = res[0];
  var phash = res[1];
  var data = {
    'seq':'0000',
    'data': scramdata,
    'payload': pload,
    'hash': phash,
  }
  var jdata = JSON.stringify(data);
  console.log("Final jdata : "+jdata);
  var inError = function(status,errortext){
    console.log("inError");
    console.log(status);
    console.log(errortext);
    if (status!= 401) {
      window.localStorage.removeItem("moffas.scram.nonce");
      window.localStorage.removeItem("moffas.scram.clientfirst");
      window.localStorage.removeItem("moffas.scram.serverfirst");
    }
    if (status == 400) {
      console.log("malformed data");

      onError(Error("malformed data"));
    }
    else if (status == 401) {
      console.log("unauthenticated");
      onError(Error("unauthenticated"));
    }
    else if (status == 403) {
      console.log("Denied");
      onError(Error("denied"));
    }
    else if (status = "parseerror") {
      console.log("Invalid response");
      onError(Error("invalid response format"));
    }
    else {
      console.log("error : "+status+":"+errortext);
      onError(Error("error : "+status+":"+errortext));
    }
  }

  var inSuccess = function(data) {
    console.log("inSuccess");
    console.log(data);
    //var data  = JSON.parse(msg);

    var serverfinal =data.response;
    var ssfinal = "v="+ss;
    var moffastoken = data.token;
    console.log("server final  : "+serverfinal);
    console.log("calculated ss : "+ssfinal);
    if (serverfinal == ssfinal) {
      console.log("final success");

      window.localStorage.removeItem("moffas.scram.nonce");
      window.localStorage.removeItem("moffas.scram.clientfirst");
      window.localStorage.removeItem("moffas.scram.serverfirst");

      window.localStorage.setItem("moffas.token",moffastoken);
      window.localStorage.setItem("moffas.seq","0");
      window.localStorage.setItem("moffas.key",aesk64);
      onSuccess(data);
    }
    else {
      window.localStorage.removeItem("moffas.scram.nonce");
      window.localStorage.removeItem("moffas.scram.clientfirst");
      window.localStorage.removeItem("moffas.scram.serverfirst");
      onError(Error("invalid server signature"));
    }
  }

  // EXECUTE HTTP REQUEST
  var exechttp = function(access_token) {
    var xhr = new XMLHttpRequest();
    var onReady = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          var response = xhr.responseText;
          var tocontinue = true;
          console.log("response :"+response);
          try {
            var j = JSON.parse(response);
          } catch(e) {
            console.log(e); // error in the above string (in this case, yes)!
            tocontinue=false;
            inError("parseerror","");
          }
          if (tocontinue) {
            inSuccess(j);
          }
        }
        else {
          inError(this.status,"HTTP ERROR");
        }
      }
    }
    xhr.onreadystatechange = onReady;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("authorization", "Bearer "+access_token);
    xhr.setRequestHeader("content-type", "application/json");
    var head = moffas.util.generateHeader(url,jdata);
    for (var k in head){
        xhr.setRequestHeader(k,head[k]);
    }
    xhr.send(jdata);
  }
  moffas.scram.getAccessToken(exechttp,onError);
}

moffas.scram.getAccessToken = function(onSuccess,onError){
  console.log("ini ngambil token");
  var access_token_validity = window.localStorage.getItem("access_token_validity");
  var inSuccess = function (data) {
    access_token_validity = Date.now() + parseInt(data.expires_in);
    window.localStorage.setItem("moffas.access_token",data.access_token);
    window.localStorage.setItem("moffas.access_token_validity",access_token_validity);
    onSuccess(data.access_token);
  }

  var onReady = function() {
    if (this.readyState == 4) {
      if (this.status == 200) {
        var response = xhr.responseText;
        var tocontinue = true;
        console.log("response :"+response);
        try {
          var j = JSON.parse(response);
        } catch(e) {
          console.log(e); // error in the above string (in this case, yes)!
          e.moffas_src = "do";
          tocontinue=false;
          onError(Error("parseerror"));
        }
        if (tocontinue) {
          inSuccess(j);
        }
      }
      else {
        onError(Error("HTTP ERROR : "+this.status));
      }
    }
  }
  if (access_token_validity==null||parseInt(access_token_validity)+60 > Date.now()) {
    var id = moffas.config.client_id;
    var password = moffas.config.client_secret;
    var url = moffas.config.oauth_url;
    var xhr = new XMLHttpRequest();

    var Authorizationhead = btoa(id+":"+password);
    xhr.onreadystatechange = onReady;
    xhr.withCredentials=true
    xhr.open("POST", url, true);
    xhr.setRequestHeader("authorization", "Basic "+Authorizationhead);
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhr.send("grant_type=client_credentials");
  }
  else {
    var access_token = window.localStorage.getItem("moffas.access_token");
    onSuccess(access_token);
  }
}
