import Vue from 'vue'
import Router from 'vue-router'
import qr_scan from '../views/qr_scan.vue'
import redir from '../views/Redir.vue'
import mainmenu from '../views/Mainmenu.vue'
import login from '../views/Login.vue'
import regis from '../views/Register.vue'
import regisnumber from '../views/RegisterNumber.vue'
import cregis from '../views/ConfirmRegister.vue'
import endregis from '../views/EndRegistration.vue'
import addtrans from '../views/AddTransaction.vue'
import qrtrans from '../views/QRTrans.vue'
import transSucccess from '../views/TransactionSuccess.vue'
import transHistory from '../views/TransactionHistory.vue'
import transDetail from '../views/TransactionDetail.vue'
import refund from '../views/RefundTransaction.vue'
import refundConfirm from '../views/refundConfirm.vue'
import payment from '../views/Payment.vue'
import prelog from '../views/Prelog.vue'
import pin from '../views/Pin.vue'
import clogin from '../views/ConfirmLogin.vue'
import topup from '../views/topup.vue'
import transfer from '../views/Transfer.vue'
import ctransfer from '../views/confirmTransfer.vue'
import pintransfer from '../views/TransferPin.vue'
import about from '../views/about.vue'
import payamount from '../views/payamount.vue'
import cpayment from '../views/ConfirmPayment.vue'


Vue.use(Router)

export default new Router({
  routes: [
    { path: '/',name: 'redir',component: redir },
    { path: '/cpayment/:pload',name: 'cpayment',component: cpayment, props:true ,meta:{title:'KONFIRMASI PEMBAYARAN'} },
    { path: '/payamount/:pload',name: 'payamount',component: payamount, props:true },
    { path: '/qr_scan',name: 'qr_scan',component: qr_scan },
    { path: '/prelog',name: 'prelog',component: prelog },
    { path: '/home',name:'D-Money', component: mainmenu, meta:{title:'D-Money'} },
    { path: '/regisnumber',name:'pendaftaran merchant', component: regisnumber, meta:{title:'pendaftaran merchant'} },
    { path: '/regis/:phone',name:'pendaftaran merchant ',props:true, component: regis, meta:{title:'pendaftaran merchant'} },
    { path: '/cregis/:pload',name:'pendaftaran merchant  ', component: cregis, props: true , meta:{title:'pendaftaran merchant'}},
    { path: '/endregis', name:'end registration',component: endregis, meta:{title:'pendaftaran merchant',showback:false} },
    { path: '/addtrans', component: addtrans, name:'Transaksi', meta:{title:'Add Transaction',showback:true}  },
    { path: '/qrtrans/:amount', component: qrtrans,props:true, name:'Transaksi ', meta:{title:'Add Transaction',showback:true}  },
    { path: '/transSucccess/', component: transSucccess ,name:'Berhasil', meta:{title:'Berhasil',showback:false}  },
    { path: '/transHistory/', component: transHistory ,name:'History Transaksi', meta:{title:'History Transaksi',showback:true}  },
    { path: '/transDetail/:trans_id', component: transDetail,props:true, name:'Detail Transaksi', meta:{title:'Detail Transaksi ',showback:true}  },
    { path: '/refund/:trans_id', component: refund,props:true, name:'Refund', meta:{title:'Refund',showback:true}  },
    { path: '/refundConfirm/:trans_id', component: refundConfirm,props:true, name:'Refund ', meta:{title:'Refund',showback:true}  },
    { path: '/pin', component: pin },
    { path: '/clogin', component: clogin },
    { path: '/topup', name:'TOP UP',component: topup, meta:{title:'TOP UP'} },
    { path: '/transfer', name:'TRANSFER',component: transfer },
    { path: '/ctransfer', name:'KONFIRMASI TRANSFER',component: ctransfer },
    { path: '/pintransfer', name:'KONFIRMASI TRANSFER2',component: pintransfer, meta:{title:'KONFIRMASI TRANSFER'} },
    { path: '/profile', component: about, meta: {back: 'home'} },
    { path: '/payment/:payload',name:'pembayaran', component: payment },
    { path: '/login',name: 'Selamat Datang',  component: login, meta:{title:'Selamat Datang'} },
  ]
})
