import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import Cordova from './Cordova.js'

import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'
import Preloader from './views/Preloader.vue'

sync(store, router)

// Load Vue instance
Vue.component('my-preloader', Preloader);

export default new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
  mounted () {
    Cordova.initialize()
  },
   beforeRouteUpdate (to, from, next) {
    // react to route changes...
    // don't forget to call next()
     console.log(to)
     next()
  }
})
