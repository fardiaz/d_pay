var moffas = moffas || {};

moffas.clear_image_data = function(prefix) {
  console.log("moffas_clear_image");
  window.localStorage.removeItem(prefix + "_long");
  window.localStorage.removeItem(prefix + "_lat");
  window.localStorage.removeItem(prefix + "_acc");
  window.localStorage.removeItem(prefix + "_path");
  window.localStorage.removeItem(prefix + "_timestamp");
  window.localStorage.removeItem(prefix + "_mimetype");
  window.localStorage.removeItem(prefix + "_updated");
};

moffas.do_request = function(op, payload, onSuccess, onError) {
    var url = moffas.config.url+"/do";
  var token = window.localStorage.getItem("moffas.token");
  var iseq = parseInt(window.localStorage.getItem("moffas.seq")) + 1;
  window.localStorage.setItem("moffas.seq", iseq);
  var aesk64 = window.localStorage.getItem("moffas.key");
  var seq = iseq.toString(16);
  console.log("Do start!");
  console.log("Op : " + op);
  console.log("seq : " + seq);
  console.log("plain payload : " + payload);

  //var pstr = JSON.stringify(payload); 
  var imsg = { op: op, seq: seq, payload: payload }
  var pstr = JSON.stringify(imsg);
  var res = moffas.util.encrypt(pstr, aesk64, seq);
  var epload = res[0];
  var hash64 = res[1];
  console.log("encrypted payload : [" + res[0] + "]");
  console.log("hash : " + res[1]);

  var msg = { token: token, seq: seq, payload: epload, hash: hash64 };
  var jdata = JSON.stringify(msg);
  console.log("msg : " + jdata);

  var inError = function(status, errortext) {
    console.log("inError");
    console.log(status);
    console.log(errortext);
    if (status == 400) {
      console.log("malformed data");
      onError(Error("malformed data"));
    } else if (status == 401) {
      console.log("unauthenticated");
      onError(Error("unauthenticated"));
    } else if (status == 403) {
      console.log("Denied");
      onError(Error("denied"));
    } else if ((status = "parseerror")) {
      console.log(Error("Invalid response"));
      onError(Error("invalid response format"));
    } else {
      console.log(Error("error : " + status + ":" + errortext));
      onError(Error("error : " + status + ":" + errortext));
    }
  };

  var inSuccess = function(data) {
    console.log("inSuccess");
    console.log(data);
    var hash64 = data.hash;
    var pload = moffas.util
      .decrypt(data.payload, aesk64, seq, hash64)
      .toString(CryptoJS.enc.Utf8);
    if (pload) {
      console.log("pload : " + pload);
      //var da  = JSON.parse(pload);
      onSuccess(pload);
    } else {
      onError(Error("invalid Hash"));
    }
  };

  // EXECUTE HTTP REQUEST
  var exechttp = function(access_token) {
    var xhr = new XMLHttpRequest();
    var onReady = function() {
      if (this.readyState == 4) {
        if (this.status == 200) {
          var response = xhr.responseText;
          var tocontinue = true;
          console.log("response :" + response);
          try {
            var j = JSON.parse(response);
          } catch (e) {
            console.log(e); // error in the above string (in this case, yes)!
            tocontinue = false;
            inError("parseerror", "");
          }
          if (tocontinue) {
            inSuccess(j);
          }
        } else {
          inError(this.status, "HTTP ERROR");
        }
      }
    };
    xhr.onreadystatechange = onReady;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Authorization", "Bearer "+access_token);
    xhr.setRequestHeader("Content-type", "application/json");
    var head = moffas.util.generateHeader(url,jdata);
    for (var k in head){
        xhr.setRequestHeader(k,head[k]);
    }
    xhr.send(jdata);
  };
  moffas.scram.getAccessToken(exechttp, onError);
};

moffas.camera = function(prefix, onSuccess, onError) {
  console.log("moffas_camera");
  var camOption = {
    quality: 50,
    destinationType: Camera.DestinationType.FILE_URI,
    allowEdit: false,
    saveToPhotoAlbum: true,
    correctOrientation: true,
    encodingType: Camera.EncodingType.JPEG,
    mediaType: Camera.MediaType.PICTURE,
    sourceType: Camera.PictureSourceType.CAMERA
  };

  navigator.camera.getPicture(onSuccess_i, onFail, camOption);
  function onSuccess_i(imageURL) {
    moffas.util.getPosition(proceed, onError);

    function proceed(p) {
      resolveLocalFileSystemURL(
        imageURL,
        function(entry) {
          var d = new Date();
          var n = d.getTime();
          function cf_win(file) {
            var reader = new FileReader();
            reader.onloadend = function(evt) {
              console.log("read success");
              var md5sum = md5(new Uint8Array(evt.target.result));
              window.localStorage.removeItem(prefix + "_long");
              window.localStorage.removeItem(prefix + "_lat");
              window.localStorage.removeItem(prefix + "_acc");
              window.localStorage.removeItem(prefix + "_path");
              window.localStorage.removeItem(prefix + "_timestamp");
              window.localStorage.removeItem(prefix + "_mimetype");
              window.localStorage.removeItem(prefix + "_updated");
              window.localStorage.setItem(prefix + "_lat", p.coords.latitude);
              window.localStorage.setItem(prefix + "_long", p.coords.longitude);
              window.localStorage.setItem(prefix + "_acc", p.coords.accuracy);
              window.localStorage.setItem(prefix + "_timestamp", n);
              window.localStorage.setItem(prefix + "_path", imageURL);
              window.localStorage.setItem(prefix + "_mimetype", "image/jpeg");
              window.localStorage.setItem(prefix + "_hash", md5sum);
              window.localStorage.setItem(prefix + "_updated", "1");
              console.log("success path: " + imageURL);
              onSuccess(imageURL);
            };
            reader.readAsArrayBuffer(file);
          }
          function cf_fail(error) {
            error.moffas_src = "FileReader";
            onError(error);
          }
          entry.file(cf_win, cf_fail);
        },
        function(error) {
          error.moffas_src = "FileResolve";
          onError(error);
        }
      );
    }
  }
  function onFail(message) {
    alert("Failed because: " + message);
    message.moffas_src = "camera";
    onError(Error(message));
  }
};

moffas.gallery = function(prefix, onSuccess, onError) {
  console.log("moffas_gallery");
  var camOption = {
    quality: 50,
    destinationType: Camera.DestinationType.FILE_URI,
    allowEdit: false,
    saveToPhotoAlbum: true,
    correctOrientation: true,
    encodingType: Camera.EncodingType.JPEG,
    mediaType: Camera.MediaType.PICTURE,
    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM
  };
  navigator.camera.getPicture(onSuccess_i, onFail, camOption);
  function onSuccess_i(imageURL) {
    resolveLocalFileSystemURL(
      imageURL,
      function(entry) {
        var d = new Date();
        var n = d.getTime();
        function cf_win(file) {
          var reader = new FileReader();
          reader.onloadend = function(evt) {
            console.log("read success");
            var md5sum = md5(new Uint8Array(evt.target.result));

            window.localStorage.removeItem(prefix + "_long");
            window.localStorage.removeItem(prefix + "_lat");
            window.localStorage.removeItem(prefix + "_acc");
            window.localStorage.removeItem(prefix + "_path");
            window.localStorage.removeItem(prefix + "_timestamp");
            window.localStorage.removeItem(prefix + "_mimetype");
            window.localStorage.removeItem(prefix + "_updated");
            window.localStorage.setItem(prefix + "_timestamp", n);
            window.localStorage.setItem(prefix + "_path", imageURL);
            window.localStorage.setItem(prefix + "_mimetype", "image/jpeg");
            window.localStorage.setItem(prefix + "_hash", md5sum);
            window.localStorage.setItem(prefix + "_updated", "1");
            console.log("success path: " + imageURL);
            onSuccess(imageURL);
          };

          reader.readAsArrayBuffer(file);
        }
        function cf_fail(error) {
          error.moffas_src = "FileReader";
          onError(error);
        }
        entry.file(cf_win, cf_fail);
      },
      function(error) {
        error.moffas_src = "FileResolve";
        onError(error);
      }
    );
  }

  function onFail(message) {
    alert("Failed because: " + message);
    message.moffas_src = "gallery";
    onError(Error(message));
  }
};

moffas.login_start = function(userid, onProceed, onUnauth, onDenied, onError) {
  console.log("moffas login");
	inError = function(err) {
	  if (err.message == "denied") {
		  onDenied();
	  }else if(err.message == "unauthenticated"){
      onUnauth();
    }else  {
		  onError(err);
	  }
	}
	moffas.scram.start(userid,"login",onProceed,inError);
};

moffas.forget_start = function(onProceed,OnUnauth,onError) {

};

moffas.regis_start = function(userid, onProceed, onDenied, onError) {
	console.log("moffas register");
	var inError = function(err) {
	  if (err.message == "denied") {
		  onDenied();
	  }
	  else  {
		  onError(err);
	  }
	}
	moffas.scram.start(userid,"register",onProceed,inError);
};

moffas.start = function(onProceed, onLogin, onError) {
	var inSuccess = function() {
	  console.log("moffas_start_session");
	  var token = window.localStorage.getItem("moffas.token");
	  if (!token) {
		  onLogin();
		  return;
	  }
	 
	  var inError = function(err) {
		  if (err.message == "unauthenticated" || err.message == "denied") {
			  onLogin();
		  }
		  else  {
			  onError(err);
		  }
	  }
	  moffas.scram.start(token,"login",onProceed,inError);
	}
	moffas.init(inSuccess,onError);
};

moffas.fin = function(otp, password, payload, onSuccess, onWrongPassword, onError) {
  var fb_token = window.localStorage.getItem("moffas.firebase_token");
  var app_id = window.localStorage.getItem("moffas.appId");
  if (fb_token == null) {
    fb_token = "dummy_token";
  }
  var deviceuuid = device.uuid;
  payload.fcm_token = fb_token,
  payload.deviceuuid = device.uuid;
  var inError = function(err) {
	  if (err.message == "unauthenticated" || err.message == "denied") {
		  onWrongPassword();
	  }
	  else  {
		  onError(err);
	  }
  }
  moffas.scram.finalize(otp,password,payload,onSuccess,inError);
};

moffas.init = function(onSuccess,onError) {
  console.log("moffas init");
  //get fcm token
  var refreshError = function(error) {
    console.error("fcm_token Refresh error.");
    console.error(error);
  };
  
  var tokenRefreshed = function(token) {
    window.localStorage.setItem("moffas.fcm_token", token);
    console.log("fcm_token: " + fcm_token);
  };
  
  var tokenRetrieved = function(token) {
    window.localStorage.setItem("moffas.fcm_token", token);
    console.log("fcm_token: " + fcm_token);
    onSuccess();
  };
  
  var retrieveError = function(error) {
    console.error("fcm_token retrieval error.");
    console.error(error);
    onError(Error("fcm_token error"));
  };

  window.localStorage.setItem("moffas.fcm_token", "token");
  onSuccess();
  // var fcm_token = window.localStorage.getItem("moffas.fcm_token");
  // if (fcm_token == null || fcm_token == "" || fcm_token == undefined) {
  //   window.FirebasePlugin.onTokenRefresh(tokenRetrieved, refreshError);
  //   window.FirebasePlugin.getToken(tokenRetrieved, retrieveError);
  // } else {
  //   onSuccess();
  // }
};

moffas.handleQR = function(result) {
        console.log("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);
          alert("We got a barcode\n" +
                "Result: " + result.text + "\n" +
                "Format: " + result.format + "\n" +
                "Cancelled: " + result.cancelled);
};

moffas.qrError = function(error) {
                console.log("scan fail");
              alert("Scanning failed: " + error);
};
    

moffas.qr_scan = function() {
// Make the webview transparent so the video preview is visible behind it.
    QRScanner.show(function(status){
      console.log(status);
    });
    QRScanner.scan(displayContents);
    function displayContents(err, text){
      if(err){
        // an error occurred, or the scan was canceled (error code `6`)
        console.log(err);
      } else {
        // The scan completed, display the contents of the QR code:
        alert(text);
        console.log(err);
      }
    }
}

